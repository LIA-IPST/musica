# Musica

Diferentes maneras de generar música

* Midi2MQTT.py: crea una puerta virtual MIDI (conectar aquí un teclado) y lo que recibe lo envía a un servidor MQTT
* MQTT2Midi.py: crea una puerta virtual MIDI y lo que recibe desde un tópico MQTT lo mueve a la puerta MIDI (conectar aquí un synth)