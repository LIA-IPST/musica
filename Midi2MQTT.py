#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import paho.mqtt.client as paho
import json
import queue
import rtmidi
import sys


#MQTT_SERVER = 'test.mosquitto.org'
MQTT_SERVER = '192.168.196.201'
MQTT_PORT = 1883
MQTT_TOPIC = 'rcr/demo/midi_bridge'

messages = queue.Queue( 1 )

def mqtt_on_message( client, userdata, message ):
    """Invocada al recibir mensaje MQTT en algun topico suscrito."""
    pass

def mqtt_on_connect( client, userdata, flag, rc ):
    """Invocada al conectar a servidor MQTT."""
    global MQTT_SERVER, MQTT_PORT, MQTT_TOPIC

    print( "[Midi2MQTT] Conectado a mqtt://%s:%d/%s" % ( MQTT_SERVER, MQTT_PORT, MQTT_TOPIC ) )

def midi_on_message( event, data=None ):
    """Invocada al recibir un mensaje MIDI."""
    global messages

    # si no se ha procesado el ultimo mensaje lo eliminamos
    try:
        messages.get_nowait()
    except queue.Empty:
        pass

    # agregamos el mensaje
    try:
        messages.put_nowait( ( event, data ) )
    except queue.Full:
            pass

def main( args ):
    global MQTT_SERVER, MQTT_PORT, messages

    midiIn = rtmidi.MidiIn()
    midiIn.open_virtual_port( 'Midi to MQTT' )

    mqtt_client = paho.Client()
    mqtt_client.on_connect = mqtt_on_connect
    mqtt_client.on_message = mqtt_on_message
    mqtt_client.connect( MQTT_SERVER, MQTT_PORT )
    mqtt_client.loop_start()

    midiIn.set_callback( midi_on_message )

    running = True
    while( running ):
        ( event, data ) = messages.get()
        print( event )
        mqtt_client.publish( MQTT_TOPIC, json.dumps( event ) )

    mqtt_client.loop_stop()
    mqtt_client.disconnect()
    midiOut.close_port()


# Show time
if __name__ == '__main__':
    ret = main( sys.argv )
    sys.exit( ret )
