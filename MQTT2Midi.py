#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import paho.mqtt.client as paho
import json
import queue
import rtmidi
import sys


#MQTT_SERVER = 'test.mosquitto.org'
MQTT_SERVER = '192.168.196.201'
MQTT_PORT = 1883
MQTT_TOPIC = 'rcr/demo/midi_bridge'

messages = queue.Queue( 1 )

def mqtt_on_message( client, userdata, message ):
    """Invocada al recibir mensaje MQTT en algun topico suscrito."""
    global messages

    # si no se ha procesado el ultimo mensaje lo eliminamos
    try:
        messages.get_nowait()
    except queue.Empty:
        pass

    # agregamos el mensaje
    try:
        messages.put_nowait( message )
    except queue.Full:
            pass

def mqtt_on_connect( client, userdata, flag, rc ):
    """Invocada al conectar a servidor MQTT."""
    global MQTT_SERVER, MQTT_PORT, MQTT_TOPIC

    client.subscribe( MQTT_TOPIC )
    print( "[MQTT2Midi] Esperando en mqtt://%s:%d/%s" % ( MQTT_SERVER, MQTT_PORT, MQTT_TOPIC ) )

def main( args ):
    global MQTT_SERVER, MQTT_PORT, messages

    midiOut = rtmidi.MidiOut()
    midiOut.open_virtual_port( 'MQTT to Midi' )

    mqtt_client = paho.Client()
    mqtt_client.on_connect = mqtt_on_connect
    mqtt_client.on_message = mqtt_on_message
    mqtt_client.connect( MQTT_SERVER, MQTT_PORT )
    mqtt_client.loop_start()

    running = True
    while( running ):
        message = messages.get()
        midiMsg = json.loads( message.payload )
        print( midiMsg )
        midiOut.send_message( midiMsg[0] )

    mqtt_client.loop_stop()
    mqtt_client.disconnect()
    midiOut.close_port()


# Show time
if __name__ == '__main__':
    ret = main( sys.argv )
    sys.exit( ret )
